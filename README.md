可用于android使用的ListView控件：

实现A-Z字母排序和过滤搜索功能
实现汉字转成拼音，实时查询 & 更新UI 
转自apkbus

![输入图片说明](http://git.oschina.net/uploads/images/2016/1109/124607_77fa9f0f_340525.png "在这里输入图片标题")



//我把它用到了选择城市列表中，有一些改进;由于数据很大，在搜索框中输入以及删除每个都会查询并更新UI，有点卡顿，这里取消删除时查询的操作

```
                //在MainActivity里找到mClearEditText的监听方法并做改动即可
		//根据输入框输入值的改变来过滤搜索
		mClearEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
				/**
				 *  优化:每次输入删除都会查询和更新UI，很卡，这里取消删除时查询的操作
				 */
				if(before >0 ){
					if(start == 0){
						filterData(s.toString());
					}
				}else{
					filterData(s.toString());
				}
				
			}
```

